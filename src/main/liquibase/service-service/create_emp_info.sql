
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `Employee_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Employee_info` ;

CREATE TABLE IF NOT EXISTS `Employee_info` (
  `user_id` VARCHAR(255) NOT NULL,
  `email_id` VARCHAR(255) NOT NULL,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `gender` VARCHAR(10) NOT NULL,
  `skype_id` VARCHAR(255) NULL,
  `phone_no` VARCHAR(15) NULL,
  `blood_group` VARCHAR(10) NULL,
  `date_of_birth` DATE NULL,
  `date_of_joining` DATE NULL,
  `date_of_leaving` DATE NULL,
  `role` VARCHAR(255) NULL,
  UNIQUE INDEX `email_id_UNIQUE` (`email_id` ASC),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Address` ;

CREATE TABLE IF NOT EXISTS `Address` (
  `user_id` VARCHAR(255) NOT NULL,
  `status` VARCHAR(255) NOT NULL,
  `address1` VARCHAR(255) NULL,
  `address2` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `state` VARCHAR(255) NULL,
  `country` VARCHAR(45) NULL,
  `zipcode` INT NULL,
  PRIMARY KEY (`status`, `user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Leave_table`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Leave_table` ;

CREATE TABLE IF NOT EXISTS `Leave_table` (
  `user_id` VARCHAR(255) NOT NULL,
  `absent_date` DATE NOT NULL,
  PRIMARY KEY (`user_id`, `absent_date`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `authentication`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `authentication` ;

CREATE TABLE IF NOT EXISTS `authentication` (
  `email_id` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`email_id`),
  UNIQUE INDEX `email_id_UNIQUE` (`email_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zipcode`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zipcode` ;

CREATE TABLE IF NOT EXISTS `zipcode` (
  `zipcode` INT(11) NOT NULL,
  `type` VARCHAR(255) NULL DEFAULT NULL,
  `city` VARCHAR(255) NULL DEFAULT NULL,
  `acceptable` VARCHAR(255) NULL DEFAULT NULL,
  `unacceptable` VARCHAR(1000) NULL DEFAULT NULL,
  `state` VARCHAR(255) NULL DEFAULT NULL,
  `county` VARCHAR(255) NULL DEFAULT NULL,
  `time_zone` VARCHAR(255) NULL DEFAULT NULL,
  `area_code` VARCHAR(255) NULL DEFAULT NULL,
  `lattitude` VARCHAR(255) NULL DEFAULT NULL,
  `longitude` VARCHAR(255) NULL DEFAULT NULL,
  `world_region` VARCHAR(255) NULL DEFAULT NULL,
  `country` VARCHAR(255) NULL DEFAULT NULL,
  `decommission` VARCHAR(255) NULL DEFAULT NULL,
  `estimated_population` VARCHAR(255) NULL DEFAULT NULL,
  `notes` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`zipcode`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
