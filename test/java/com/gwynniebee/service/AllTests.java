/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * All test cases.
 * @author karan
 */
@RunWith(Suite.class)
@SuiteClasses({TestLiquibaseChangeSet.class})
public class AllTests {

}
